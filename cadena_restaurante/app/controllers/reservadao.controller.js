const db = require("../models");

const Reserva = db.Reserva;


const Op = db.Sequelize.Op;

//GET
exports.findAll = (req, res) => {

    console.log("entro aqui")

    const nombre = req.query.nombre;

    var condition = nombre ? { nombre: { [Op.iLike]: `%${nombre}%` } } : null;

    Reserva.findAll({ where: null })

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ocurrio un error  obtener las mesas."

            });

        });

};

//POST
exports.create = (req,res) => {

   

        const reserva  = {
            id_restaurante:req.body.id_restaurante,
            id_mesa:req.body.id_mesa,
            fecha:req.body.fecha,
            hora_inicio:req.body.hora_inicio,
            hora_fin:req.body.hora_fin,
            id_cliente:req.body.id_cliente,
            cantidad_solicitada:req.body.cantidad_solicitada
        }
    
    
    
        //validaciones varias como verificar horas, cantidad mesas, disponilidad
    
        Reserva.create(reserva).then(
            (data) => {
                console.log(data);
                res.send(data);
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al crear una mesa."
    
                });
            } 
        );

    
        
    

    





    

}

//PUT
exports.update = (req,res) => {


    const reserva  = {
        id:req.body.id,
        id_restaurante:req.body.id_restaurante,
        id_mesa:req.body.id_mesa,
        fecha:req.body.fecha,
        hora_inicio:req.body.hora_inicio,
        hora_fin:req.body.hora_fin,
        id_cliente:req.body.id_cliente,
        cantidad_solicitada:req.body.cantidad_solicitada
    }

    const condicion = {
        where:{
            id:req.body.id
        }
    }


    Reserva.update(mesa,condicion)
    .then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al actualizar una mesa."

            });
        } 
    );
}

//DELETE
exports.destroy = (req,res) => {


        
        const condicion = {
            where:{
                id:req.body.id
            }
        }
    
    
        Mesa.destroy(condicion)
        .then(
            (data) => {
                console.log("===========================================================")
                console.log(data);
                res.send({
                    registros_eliminados: data
                });
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al borrar una mesa."
    
                });
            } 
        );
    

}

