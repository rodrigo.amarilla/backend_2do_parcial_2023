const db = require("../models");

const Restaurante = db.Restaurante;

const Op = db.Sequelize.Op;


exports.findAll = (req, res) => {

    console.log("entro aqui")

    const nombre = req.query.nombre;

    var condition = nombre ? { nombre: { [Op.iLike]: `%${nombre}%` } } : null;

    Restaurante.findAll({ where: condition })

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ocurrio un error adfadsfadsal obtener los restaurantes."

            });

        });

};


exports.create = (req,res) => {


    const restaurante = {
        nombre: req.body.nombre,
        direccion: req.body.direccion,
        createdAt: Date.now(),
        updatedAt: Date.now()
    }


    //validaciones varias como verificar horas, cantidad mesas, disponilidad

    Restaurante.create(restaurante).then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al crear un restaurante."

            });
        } 
    );


}


exports.update = (req,res) => {


    const restaurante = {
        nombre: req.body.nombre,
        direccion: req.body.direccion,
        updatedAt: Date.now()
    }

    const condicion = {
        where:{
            id:req.body.id
        }
    }


    Restaurante.update(restaurante,condicion)
    .then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al actualizar un restaurante."

            });
        } 
    );
}

exports.destroy = (req,res) => {


        
        const condicion = {
            where:{
                id:req.body.id
            }
        }
    
    
        Restaurante.destroy(condicion)
        .then(
            (data) => {
                console.log("===========================================================")
                console.log(data);
                res.send({
                    registros_eliminados: data
                });
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al borrar un restaurante."
    
                });
            } 
        );
    

}

