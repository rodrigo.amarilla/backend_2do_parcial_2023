const db = require("../models");

const Cliente = db.Cliente;

const Op = db.Sequelize.Op;

//GET
exports.findAll = (req, res) => {

    console.log("entro aqui")

    const nombre = req.query.nombre;
    const cedula = req.query.cedula;

    console.log(cedula);
    console.log(nombre);

    

    //var condition = nombre ? { nombre: { [Op.iLike]: `%${nombre}%` } } : null;

    var condicion = {}

    if(nombre){
        condicion.nombre = nombre ? { [Op.iLike]: `%${nombre}%` }:null;
    }
    

    if (cedula){
        condicion.cedula = cedula
    }

    console.log(condicion);
    
    

    Cliente.findAll({ where: condicion })

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ocurrio un error al obtener los clientes."

            });

        });

};

//POST
exports.create = (req,res) => {


    const cliente = {
        cedula: req.body.cedula,
        nombre: req.body.nombre,
        apellido: req.body.apellido
    }

console.log(Cliente);

    Cliente.create(cliente).then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al crear un cliente."

            });
        } 
    );


}

