import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  constructor(
    private http: HttpClient,
  ) { }


  getReserva(){
    console.log(this.http);
    return this.http.get('api/cadena_restaurante/api/reserva');
  }

  getRestaurante(){
    return this.http.get('api/cadena_restaurante/api/restaurante');
  }

  getMesaByRestaurante(id_restaurante:any , cantidad:any){
    return this.http.get(`api/cadena_restaurante/api/mesa?id_restaurante=${id_restaurante}&cantidad=${cantidad}`);
  }

  create(nuevo:any){

    return this.http.post(`api/cadena_restaurante/api/reserva`, nuevo);


  }

  getcliente(cedula:any){
    return this.http.get(`api/cadena_restaurante/api/cliente?cedula=${cedula}`);
  }

  createCliente(cliente:any){
    return this.http.post(`api/cadena_restaurante/api/cliente`,cliente);
  }
}
