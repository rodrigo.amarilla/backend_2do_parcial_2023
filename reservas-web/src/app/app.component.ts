
import { Component, OnInit } from '@angular/core';


import { ReservaService } from '../services/reserva.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent   implements OnInit{
  title = 'reservas-web';

  filtro:string = '';
  respuesta: any = {
    list : []
  };


  constructor(
    private reservaService: ReservaService
  ) { }

  ngOnInit() {

    this.buscar();
  }


  buscar() {
    
    this.reservaService.getReserva().subscribe(
      (data: any) => {
        
        console.log(data);

        this.respuesta.lista = data
        
        
      },
      err => {
        
        console.log(err);
      }
    );
    console.log('agregar');
  }
}
