import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearresersaComponent } from './crearresersa.component';

describe('CrearresersaComponent', () => {
  let component: CrearresersaComponent;
  let fixture: ComponentFixture<CrearresersaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CrearresersaComponent]
    });
    fixture = TestBed.createComponent(CrearresersaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
