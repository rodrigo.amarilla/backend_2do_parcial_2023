import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ReservaService } from '../../services/reserva.service';

@Component({
  selector: 'app-crearresersa',
  templateUrl: './crearresersa.component.html',
  styleUrls: ['./crearresersa.component.css']
})
export class CrearresersaComponent  implements OnInit{


  new: any = {
    id_restaurante: null ,
    id_mesa: null,
    id_cliente:null,
    cantidad_solicitada : 1
  }

  restaurantes: any = [];

  huboError:boolean = false;
  error:any = {};
  exito:any = {};

  existeCliente:boolean = false;

  cliente:any = {
    nombre:"",
    apellido:"",
    cedula:""
  };

  mesas:any = [];

  

  constructor(
    private reservaService: ReservaService
  ) { }


  ngOnInit() {

    this.reservaService.getRestaurante().subscribe(
      (data) =>{
        console.log(data);
        this.restaurantes = data;
      },
      (err)=>{
        console.log(err);
      }
    );
  }

  getMesas(){
    
    this.reservaService.getMesaByRestaurante(this.new.id_restaurante,this.new.cantidad_solicitada).subscribe(
      (data)=>{
        console.log(data);
        this.mesas = data;
      },
      (err)=>{
        console.log(err);
      }
    );
  }


  getCliente() {
    this.existeCliente = false;
    
    this.reservaService.getcliente(this.cliente.cedula).subscribe(
      (data:any)=>{
        console.log(data);
        
        if(data && data.length > 0){
          console.log("entro aqqui")
          this.cliente = data[0];
          this.existeCliente = true;
          this.new.id_cliente = this.cliente.id;
        } else {
          this.cliente.nombre = "";
          this.cliente.apellido = "";
        }
      },
      (err)=>{
        console.log(err);
      }
    );
  }

  crear(form: NgForm) {

    this.error = {};

    if(!this.existeCliente){
      const newCliente  ={
        cedula: this.cliente.cedula,
        nombre:this.cliente.nombre,
        apellido:this.cliente.apellido
      }
      console.log(this.cliente);
      console.log(newCliente);
      this.reservaService.createCliente(newCliente).subscribe(
        (data:any)=>{
          console.log(data);
          this.new.id_cliente = data.id;
          console.log(this.new);
    this.reservaService.create(this.new).subscribe(
      (data: any) => {
        
        console.log(data);

        this.exito.message = 'exito al crear el registro'
        
      },
      (err:any) => {
        
        console.log(err);

        this.error = err.error


      }
    );
          
        },
        (err)=> {
          console.log(err);
        }
      );
    } else {
      
      console.log(this.new);
    this.reservaService.create(this.new).subscribe(
      (data: any) => {
        
        console.log(data);

        this.exito.message = 'exito al crear el registro'
        
      },
      (err:any) => {
        
        console.log(err);

        this.error = err.error


      }
    );
    }


  

    

  }

}
