import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';


import { CrearresersaComponent } from './crearresersa/crearresersa.component';
import { ReservaComponent } from './reserva/reserva.component';


const routes: Routes = [
  { path: 'crear-reserva', component: CrearresersaComponent },
  { path: 'reserva', component: ReservaComponent }
];


/*@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AppRoutingModule { }*/
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
